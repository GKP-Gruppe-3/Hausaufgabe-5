#include "stdafx.h"

#include "monat.h"
#include "datum.h"

/***************************************
* Aufgabe 1.6 - Monatsberechnungen (Implementation):
***************************************/

void month_Ctor(month_t *month, int_fast16_t year, int_fast8_t month_number)
{
  month->number = month_number;
  switch (month_number)
  {
    case 1:
      month->days = 31;
      month->name = "Januar";
      break;
    case 2:
      month->days = (isLeapYear(year) ? 29 : 28);
      month->name = "Februar";
      break;
    case 3:
      month->days = 31;
      month->name = "Maerz";
      break;
    case 4:
      month->days = 30;
      month->name = "April";
      break;
    case 5:
      month->days = 31;
      month->name = "Mai";
      break;
    case 6:
      month->days = 30;
      month->name = "Juni";
      break;
    case 7:
      month->days = 31;
      month->name = "Juli";
      break;
    case 8:
      month->days = 31;
      month->name = "August";
      break;
    case 9:
      month->days = 30;
      month->name = "September";
      break;
    case 10:
      month->days = 31;
      month->name = "Oktober";
      break;
    case 11:
      month->days = 30;
      month->name = "November";
      break;
    case 12:
      month->days = 31;
      month->name = "Dezember";
      break;
      break;
  }
}
