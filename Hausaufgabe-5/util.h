#ifndef __UTIL_H__
#define __UTIL_H__

#include <stdbool.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MALLOC_SIZE(size) malloc((size_t)(size))
#define MALLOC_ARRAY(type, size) (type*) MALLOC_SIZE((size) * sizeof(type))
#define MALLOC(type) MALLOC_ARRAY(type, 1)

typedef int_fast8_t cmp_t;

#define CMP_LESS -1
#define CMP_EQUAL 0
#define CMP_GREATER 1

// Die Ein-/Ausgabe auf der Konsole pausieren,
// und anschließend die gesamte Ausgabe bereinigen.
void pause();

// Eine Zeichenkette von der Konsole lesen.
void stringGetFromConsole(char *str, int max_chars, char const* const message, ...);

// Einen "int"-Wert von der Konsole lesen.
int intGetFromConsole(char const* const message, ...);

// Einen "long"-Wert von der Konsole lesen.
long longGetFromConsole(char const* const message, ...);

// Einen "double"-Wert von der Konsole lesen.
double doubleGetFromConsole(char const* const message, ...);

#endif
