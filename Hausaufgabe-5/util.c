#include "stdafx.h"

#include "util.h"

// Die Ein-/Ausgabe auf der Konsole pausieren,
// und anschließend die gesamte Ausgabe bereinigen.
void pause()
{
  system("pause");
  system("cls");
}

// Eine Zeichenkette von der Konsole lesen.
void stringGetFromConsole(char *str, int max_chars, char const* const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }

  fgets(str, max_chars, stdin);
  size_t i = strlen(str) - 1;
  if (str[i] == '\r' || str[i] == '\n')
  {
    str[i] = '\0';
  }
}

// Einen "int"-Wert von der Konsole lesen.
int intGetFromConsole(char const* const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  int i;
  scanf_s("%d", &i);
  return i;
}

// Einen "long"-Wert von der Konsole lesen.
long longGetFromConsole(char const* const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  long l;
  scanf_s("%ld", &l);
  return l;
}

// Einen "double"-Wert von der Konsole lesen.
double doubleGetFromConsole(char const* const message, ...)
{
  if (message != NULL)
  {
    va_list arg_list;
    va_start(arg_list, message);
    vprintf(message, arg_list);
    va_end(arg_list);
  }
  double d;
  scanf_s("%lf", &d);
  return d;
}
