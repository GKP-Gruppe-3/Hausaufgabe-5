#ifndef __DATUM_H__
#define __DATUM_H__

#include "util.h"

extern const uint_fast16_t DAYS_TO_MONTH_365[];
extern const uint_fast16_t DAYS_TO_MONTH_366[];

bool isLeapYear(const int_fast16_t year);

/***************************************
* Aufgabe 1.1 - Verbund f�r Datum:
***************************************/

typedef struct date date_t;
struct date
{
  // Die Jahreszahlen m�ssen mindestens vierstellig sein,
  // und sind immer positiv.
  uint_fast16_t year;
  // Die Monatsnummerierungen k�nnen h�chstens zweistellig sein,
  // und sind immer positiv.
  uint_fast8_t month;
  // Die Tagesnummerierungen je Monat k�nnen h�chstens zweistellig sein,
  // und sind immer positiv.
  uint_fast8_t day;
};

/***************************************
* Aufgabe 1.4 - Erweiterung des Header:
***************************************/

bool date_IsValid(const date_t *const date);

cmp_t date_Compare(const date_t *const left, const date_t *const right);

bool date_Equals(const date_t *const left, const date_t *const right);

uint_fast16_t date_DayOfYear(const date_t *const date);

int_fast32_t date_DiffDays(const date_t *const left, const date_t *const right);

/***************************************
* Aufgabe 1.2 - Routinen im Header:
***************************************/

date_t date_GetFromConsole();

void date_PutToConsole(const date_t *const date, bool long_representation);

/***************************************
* Aufgabe 1.7 - Testprogramm (Header):
***************************************/

void date_Test();

#endif
