#ifndef __DATUM_POINTER_H__
#define __DATUM_POINTER_H__

#include "util.h"
#include "monat.h"

/***************************************
* Aufgabe 1.8 - Zusatzaufgabe: Datum mit Pointer (Header):
***************************************/

typedef struct date2 date2_t;
struct date2
{
  // Die Jahreszahlen m�ssen mindestens vierstellig sein,
  // und sind immer positiv.
  uint_fast16_t year;
  month_t *month;
  // Die Tagesnummerierungen je Monat k�nnen h�chstens zweistellig sein,
  // und sind immer positiv.
  uint_fast8_t day;
};

bool date2_IsValid(const date2_t *const date);

cmp_t date2_Compare(const date2_t *const left, const date2_t *const right);

bool date2_Equals(const date2_t *const left, const date2_t *const right);

uint_fast16_t date2_DayOfYear(const date2_t *const date);

int_fast32_t date2_DiffDays(const date2_t *const left, const date2_t *const right);

date2_t date2_GetFromConsole();

void date2_PutToConsole(const date2_t *const date, bool long_representation);

void date2_Dtor(date2_t *date);

void date2_Test();

#endif
