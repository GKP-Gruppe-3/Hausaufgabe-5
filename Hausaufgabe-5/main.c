#include "stdafx.h"

#include "util.h"
#include "datum.h"
#include "datum_pointer.h"

/* Testwerte je Testfunktion (mit Zeilenumbrüchen auf die Konsole kopieren):
2.Feb.2012
9.9.1999
//*/
int main()
{
  date_Test();
  date2_Test();

  return 0;
}
