# Hausaufgabe 5

## Aufgabe 1: Datumsberechnungen (38 + 4 Punkte)

### i) Verbund für Datum (3 Punkte)

Erstellen Sie eine Datei namens datum.h und definieren Sie darin eine Struktur zur
Beschreibung eines Datums. Die Struktur soll dabei aus einzelnen Werten für Tag, Monat
und Jahr bestehen. Wählen Sie dabei sinnvolle Datentypen. Begründen Sie im Quelltext
Ihre Wahl.

### ii) Routinen im Header (2 Punkte)

Ergänzen Sie die Datei datum.h um die Definition zweier Funktionen für die Eingabe
und Ausgabe eines Datums. Die Eingabefunktion soll keine Parameter besitzen, aber ein
Datum zurückgeben. Die Ausgabefunktion demgegenüber besitzt keinen Rückgabewert,
aber das auszugebende Datum ist Parameter der Funktion. Weiterhin soll die Ausgabe
ein Datum auf zwei möglichen Arten ausgeben können (01.12.2012 & 01. Dezember 2012).
Ein weiterer Funktionsparameter soll dabei die gewünschte Ausgabe beschreiben.

### iii) Implementierung (6 Punkte)

Implementieren Sie die in Teilaufgabe ii) definierten Routinen in der Datei datum.(c|cpp).
Hinweis: Die Eingabe kann einzeln die Werte für Tag, Monat und Jahr vom Benutzer
abfragen. Sie können aber auch gern eine vom Nutzer eingegebene Zeichenkette (z.B.
01.12.2012) parsen - werfen Sie dazu z.B. einen Blick auf die string.h-Bibliothek. Denken
Sie bei der Ausgabe an die zwei möglichen Arten (01.12.2012 & 01. Dezember 2012).

### iv) Erweiterung des Header (4 Punkte)

Ergänzen Sie die Datei datum.h um die weiteren folgenden Funktionen:
• Eine Funktion zur Prüfung, ob ein eingegebenes Datum ein gültiges Datum in
Bezug auf den Gregorianischen Kalender darstellt. Beachten Sie dabei vor allem
die Schaltjahr-Regelung.
• Eine Funktion, um zwei Daten auf Gleichheit zu prüfen. Sprich, stellen beide Daten
den gleichen Tag dar.
• Eine Funktion zur Berechnung des laufenden Tages im Jahr für ein übergebenes
Datum. Beispiel: 12. Februar 2012 = 43. Tag im Jahr
• Eine Funktion zur Berechnung des zeitlichen Abstandes in Tagen zwischen zwei
Daten.

### v) Erweiterung der Implementierung (12 Punkte)

Implementieren Sie diese neuen Routinen in der Datei datum.(c|cpp).

### vi) Monatsberechnungen (5 Punkte)

Definieren Sie eine Struktur für die Beschreibung eines einzelnen Monats. Die Struktur
soll neben der laufenden Nummer des Monats (1-12) auch die Anzahl an Tagen im Monat
sowie den Monatsnamen (Januar, Februar, ...) enthalten. Denken Sie dabei über die
Verwendung sinnvoller Datentypen nach. Begründen Sie Ihre Wahl im Quelltext.
Schreiben Sie eine Funktion zur Erzeugung einer solchen Struktur. Die Funktion soll dabei
als Parameter die Monatsnummer und das Jahr besitzen und einen korrekten Monat
zurückgeben.
Definieren & Implementieren Sie diese Struktur und diese Funktion in einem Modul
namens monat(.h/.(c|c pp)).

### vii) Testprogramm (6 Punkte)

Schreiben Sie ein interaktives Programm, um die geschrieben Funktionen zu testen.

### viii) Zusatzaufgabe: Datum mit Pointer (4 Punkte)

Erstellen Sie eine neue Datei datum_pointer.h. Definieren Sie darin eine neue Struktur
für ein Datum, welche im Gegensatz zur Struktur aus Teilaufgabe i) den Monatswert
durch einen Pointer auf Ihren in Teilaufgabe vi) definierten Strukturtyp darstellt.
Welche möglichen Vereinfachungen für Ihre in den vorherigen Aufgaben geschriebenen
Funktionen ergeben sich dadurch? Beantworten Sie die Frage im Quelltext.
